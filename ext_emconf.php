<?php

$EM_CONF['randomizer'] = [
    'title' => 'Randomizer',
    'description' => 'This is a TYPO3 extension. It randomizes content in the database or during an export. It\'s a wrapper for fakerphp/faker.',
    'version' => '3.5.0',
    'category' => 'be',
    'state' => 'stable',
    'clearcacheonload' => true,
    'author' => 'Marco Huber',
    'author_email' => 'mail@marco-huber.de',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-12.4.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
