<?php

namespace Mhuber84\Randomizer\Command;

use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class RandomizeCommand
 * Based on Georg Ringer's https://github.com/georgringer/gdpr/blob/master/Classes/Command/RandomizeCommand.php
 */
class RandomizeCommand extends AbstractCommand
{

    protected static $defaultName = 'randomizer:randomize';

    /**
     * @var bool
     */
    protected $isDryRun;

    /**
     * Configure the command by defining the name, options and arguments
     */
    protected function configure()
    {
        parent::configure();
        $this
            ->setDescription('Randomize data')
            ->addOption('no-dry-run', null, InputOption::VALUE_NONE, 'By default, the database is not changed ("dry-run"). Set this option, to execute the database changes.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);

        $this->isDryRun = true;
        if ($input->getOption('no-dry-run')) {
            $this->isDryRun = false;
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @throws \InvalidArgumentException
     * @throws \TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var SymfonyStyle $io */
        $io = new SymfonyStyle($input, $output);
        $io->title($this->getDescription());

        if ($input->getOption('show-config')) {
            $io->writeln('Current configuration:');
            $io->writeln(print_r($this->configuration, true));
            return 0;
        }
        if (!is_array($this->configuration['faker.']['mapping.'])) {
            $io->writeln('No mapping configured!');
            return 1;
        }

        if ($this->isDryRun) {
            $io->writeln('DRY-RUN!');
        }

        foreach ($this->configuration['faker.']['mapping.'] as $table => $fieldToFakerMapping) {
            $tableName = substr($table, 0, -1);

            $newRows = [];
            $newRowsCount = 0;
            $this->randomizeTable($tableName, $fieldToFakerMapping, $io, $output, $newRows, $newRowsCount);

            $io->writeln(($this->isDryRun ? 'DRY-RUN! ' : '') . 'Start updating database.');
            $progressBar = new ProgressBar($output, $newRowsCount, 0.5);
            $progressBar->start();
            $updatedRows = $this->updateTable($tableName, $newRows, $progressBar);
            $progressBar->finish();
            $io->newLine();
            $io->writeln(($this->isDryRun ? 'DRY-RUN! ' : '') . $updatedRows . ' records updated.');
        }
        $io->writeln('Finished.');
        return 0;
    }

    /**
     * @param string $table
     * @param array $newRows
     * @param ProgressBar $progressBar
     * @return int
     */
    protected function updateTable($table, $newRows, &$progressBar)
    {
        $updatedRows = 0;
        $queryBuilderForConnection = $this->connectionPool->getConnectionForTable($table);
        foreach ($newRows as $newRow) {
            if ($this->isDryRun === false && $newRow['uid']) {
                $queryBuilderForConnection
                    ->update(
                        $table,
                        $newRow,
                        [
                            'uid' => $newRow['uid']
                        ]
                    );
            }
            $progressBar->advance();
            $updatedRows++;
        }
        return $updatedRows;
    }
}
