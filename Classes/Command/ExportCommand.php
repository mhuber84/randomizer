<?php

namespace Mhuber84\Randomizer\Command;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Types\Types;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class ExportCommand
 */
class ExportCommand extends AbstractCommand
{

    protected static $defaultName = 'randomizer:export';

    /**
     * Configure the command by defining the name, options and arguments
     */
    protected function configure()
    {
        parent::configure();
        $this
            ->setDescription('Export randomized data')
            ->addArgument('target-file', InputArgument::REQUIRED, 'Target output file, f.e. /path/to/mydump.sql')
            ->addOption('full-export', null, InputOption::VALUE_NONE, 'Default is, to export only configured tables. With this option, you can create a full database dump');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @throws \InvalidArgumentException
     * @throws \TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var SymfonyStyle $io */
        $io = new SymfonyStyle($input, $output);
        $io->title($this->getDescription());

        if ($input->getOption('show-config')) {
            $io->writeln('Current configuration:');
            $io->writeln(print_r($this->configuration, true));
            return 0;
        }
        if (!is_array($this->configuration['faker.']['mapping.'])) {
            $io->writeln('No mapping configured!');
            return 1;
        }

        $schemaManager = $this->connectionPool->getConnectionForTable('tt_content')->getSchemaManager();

        $fakerTables = array_keys($this->configuration['faker.']['mapping.']);
        if ($input->getOption('full-export')) {
            $io->writeln('Full database export!');
        }
        $allTables = $schemaManager->listTables();

        $sql = '';
        foreach ($allTables as $table) {
            $tableName = $table->getName();
            if (!$input->getOption('full-export') && !in_array($tableName . '.', $fakerTables)) {
                continue;
            }
            $fieldToFakerMapping = [];
            if (isset($this->configuration['faker.']['mapping.'][$tableName . '.'])) {
                $fieldToFakerMapping = $this->configuration['faker.']['mapping.'][$tableName . '.'];
            }

            $newRows = [];
            $newRowsCount = 0;
            $this->randomizeTable($tableName, $fieldToFakerMapping, $io, $output, $newRows, $newRowsCount);

            $io->writeln('Start creating SQL string');
            $progressBar = new ProgressBar($output, $newRowsCount, 0.5);
            $progressBar->start();
            $sql .= $this->createSQL($table, $newRows, $progressBar);
            $progressBar->finish();
        }

        $io->newLine();
        $io->writeln('Write string to file ' . $input->getArgument('target-file'));
        GeneralUtility::writeFile($input->getArgument('target-file'), $sql);

        $io->writeln('Finished.');
        return 0;
    }

    /**
     * @param Table $table
     * @param array $newRows
     * @param ProgressBar $progressBar
     * @return string
     */
    protected function createSQL($table, $newRows, &$progressBar)
    {
        $connection = $this->connectionPool->getConnectionForTable($table->getName());
        $queryBuilder = $this->connectionPool->getQueryBuilderForTable($table->getName());
        $sql = '';
        $sql .= 'DROP TABLE IF EXISTS ' . $queryBuilder->quoteIdentifier($table->getName()) . ";\n";

        $createFlags = AbstractPlatform::CREATE_INDEXES|AbstractPlatform::CREATE_FOREIGNKEYS;
        $sql .= implode(";\n", $connection->getDatabasePlatform()->getCreateTableSQL($table, $createFlags)) . ";\n";

        $sql .= 'LOCK TABLE ' . $queryBuilder->quoteIdentifier($table->getName()) . " WRITE;\n";
        foreach ($newRows as $values) {
            $progressBar->advance();
            $queryBuilder->insert($table->getName());
            foreach ($values as $column => $value) {
                if ($value === null) {
                    $queryBuilder->setValue($column, 'NULL', false);
                } else {
                    $columnType = $table->getColumn($column)->getType()->getName();
                    switch ($columnType) {
                        case Types::INTEGER:
                            $queryBuilder->setValue($column, (int)$value, false);
                            break;
                        case Types::FLOAT:
                            $queryBuilder->setValue($column, (double)$value, false);
                            break;
                        default:
                            $queryBuilder->setValue($column, $queryBuilder->quote($value), false);
                            break;
                    }
                }
            }
            $sql .= $queryBuilder->getSQL() . ";\n";
        }
        $sql .= 'UNLOCK TABLES ' . ";\n";

        return $sql;
    }
}
