<?php

namespace Mhuber84\Randomizer\Command;

use Faker\Factory;
use Faker\Generator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class ExportCommand
 */
class AbstractCommand extends Command
{

    /**
     * @var Generator
     */
    protected $faker;

    /**
     * @var ConnectionPool
     */
    protected $connectionPool;

    /**
     * @var array
     */
    protected $configuration;

    /**
     * Configure the command by defining the name, options and arguments
     */
    protected function configure()
    {
        $this
            ->addOption('show-config', null, InputOption::VALUE_NONE, 'Shows the configuration and exit. Does not execute the export.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);

        $this->configuration = $this->getExtensionConfiguration();
        $locale = $this->configuration['faker.']['locale'] ? $this->configuration['faker.']['locale'] : Factory::DEFAULT_LOCALE;
        $this->faker = Factory::create($locale);
        $this->connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        foreach ($this->configuration['faker.']['providers.'] as $provider) {
            $this->faker->addProvider(new $provider($this->faker));
        }
    }

    /**
     * @return array
     */
    protected function getExtensionConfiguration()
    {
        $extSettings = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('randomizer');
        return $extSettings;
    }

    /**
     * @param string $table
     * @param string $databaseFieldName
     * @param string $fakerFormatter
     * @param bool $resetUnique
     * @return mixed
     */
    protected function randomizeValue($table, $databaseFieldName, $fakerFormatter, $resetUnique = false)
    {
        try {
            if (
                isset($this->configuration['faker.']['unique.'][$table . '.'][$databaseFieldName]) &&
                (bool)$this->configuration['faker.']['unique.'][$table . '.'][$databaseFieldName] == true
            ) {
                $newValue = $this->faker->unique($resetUnique)->$fakerFormatter;
            } else {
                $newValue = $this->faker->$fakerFormatter;
            }
        } catch (\InvalidArgumentException $e) {
            throw new \InvalidArgumentException(sprintf(
                'Exception %s for mapping of field "%s" of table "%s"',
                $e->getMessage(),
                $databaseFieldName,
                $table
            ), 1589283588);
        }
        return $newValue;
    }

    protected function randomizeTable($tableName, $fieldToFakerMapping, &$io, &$output, &$newRows, &$newRowsCount)
    {
        $io->newLine(2);
        $io->writeln('Start processing table ' . $tableName . '.');

        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $this->connectionPool->getQueryBuilderForTable($tableName);
        $queryBuilder->getRestrictions()->removeAll();

        $recordCount = $queryBuilder
            ->count('*')
            ->from($tableName)
            ->executeQuery()
            ->fetchOne();
        $io->writeln($recordCount . ' records for table ' . $tableName . ' found.');

        $io->writeln('Start creating randomized values.');
        $progressBar = new ProgressBar($output, $recordCount, 0.5);
        $progressBar->start();
        $res = $queryBuilder
            ->select('*')
            ->from($tableName)
            ->executeQuery();
        while ($databaseRow = $res->fetch()) {
            $newValues = [];
            foreach ($databaseRow as $databaseFieldName => $databaseField) {
                $newValues[$databaseFieldName] = $databaseField;
                if ($databaseField !== NULL && strlen($databaseField) > 0 && isset($fieldToFakerMapping[$databaseFieldName])) {
                    $newValue = $this->randomizeValue($tableName, $databaseFieldName, $fieldToFakerMapping[$databaseFieldName], $newRowsCount === 0);
                    $newValues[$databaseFieldName] = $newValue;
                }
            }
            if (isset($this->configuration['equal.'][$tableName . '.'])) {
                $equalFieldsConfiguration = $this->configuration['equal.'][$tableName . '.'];
                foreach ($databaseRow as $databaseFieldName => $databaseField) {
                    if (
                        isset($equalFieldsConfiguration[$databaseFieldName]) &&
                        isset($newValues[$equalFieldsConfiguration[$databaseFieldName]])
                    ) {
                        $newValue = $newValues[$equalFieldsConfiguration[$databaseFieldName]];
                        $newValues[$databaseFieldName] = $newValue;
                    }
                }
            }
            if (count($newValues) > 0) {
                $newRows[] = $newValues;
                $newRowsCount++;
            }
            $progressBar->advance();
        }
        $progressBar->finish();
        $io->newLine();
        $io->writeln('Randomized values for ' . $newRowsCount . ' records created.');
    }
}
