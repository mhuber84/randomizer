<?php

namespace Mhuber84\Randomizer\FakerProviders;

use Faker\Provider\Base;

class ExampleProvider extends Base
{

    /**
     * @return false|string
     */
    public function exampleFaker()
    {
        // generate crazy random stuff
        return time();
    }
}
