<?php

namespace Mhuber84\Randomizer\FakerProviders;

use Faker\Provider\Base;
use Faker\Provider\Lorem;

class ArrayProvider extends Base
{

    /**
     * @param int $words
     * @return string
     */
    public function serialized($words = 3)
    {
        $loremProvider = new Lorem($this->generator);
        $arrayKeys = $loremProvider->words($words);
        $resultArray = [];
        foreach ($arrayKeys as $key) {
            $resultArray[$key] = $loremProvider->word();
        }
        return serialize($resultArray);
    }

    /**
     * @param int $words
     * @return string
     */
    public function json($words = 3)
    {
        $loremProvider = new Lorem($this->generator);
        $arrayKeys = $loremProvider->words($words);
        $resultArray = [];
        foreach ($arrayKeys as $key) {
            $resultArray[$key] = $loremProvider->word();
        }
        return json_encode($resultArray);
    }
}
