# Randomizer TYPO3 Extension

This is a TYPO3 extension. It randomizes content in the database. It's a wrapper for fakerphp/faker.

## What does it do?

With this extension you can randomize content in database tables or during an database export.

It's useful to anonymize data on/for development systems.
The random values are generated with the fakerphp/faker package.

### Randomize in Database

If a configured field in a configured table has a value, the value is overwritten with a random generated value.
Empty fields are ignored.

### Randomize during export

If a configured field in a configured table has a value, the value is overwritten with a random generated value.
By default, only configured tables are exported. But you can export the whole database, too.

## Installation

Install the extension by using composer: `composer req mhuber84/randomizer`. If you want to use the export feature,
you have to install it in production. Else you can add the option `--dev` to install it on dev systems only.
It should work with TYPO3 8.6, 9.5 and 10.4.

## Configuration

Configure the tables and fields in your site package ext_localconf.php. or the AdditionalConfiguration.php.

This is an example  configuration for the fe_users table.
Available faker providers/formatters are listed at https://fakerphp.github.io/formatters/.
An additional Array faker provider is `\Mhuber84\Randomizer\FakerProviders\ArrayProvider`. It can return a serialzed
and json array. It is used for the field 'uc' in this example.

The tables you want to randomize must have a unique column namend "uid"!

```php
$randomizerSettings = $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['randomizer'];

// Database fields to formatter mapping. https://fakerphp.github.io/formatters/
$randomizerSettings['faker.']['mapping.']['fe_users.'] = [
    //TYPO3 Core fields
    'username' => 'userName',
    'password' => 'password',
    'address' => 'streetAddress',
    'telephone' => 'phoneNumber',
    'fax' => 'phoneNumber',
    'email' => 'safeEmail',
    'uc' => 'serialized',
    'zip' => 'postcode',
    'city' => 'city',
    'country' => 'countryCode',
    'www' => 'url',
    'company' => 'company',
    //'image' => '', //TODO
    'first_name' => 'firstName',
    'last_name' => 'lastName',
    'middle_name' => 'firstName',
    'felogin_forgotHash' => 'md5',
];

// List of unique fields. Fields have to be defined in the mapping, too! https://fakerphp.github.io/#modifiers
$randomizerSettings['faker.']['unique.']['fe_users.'] = [
    //TYPO3 Core fields
    'username' => true,
];

// Locale for generated values. https://fakerphp.github.io/#localization
$randomizerSettings['faker.']['locale'] = 'de_DE';

// List of equal fields.
// This has nothing to do with faker, but is useful for fe_users table if username and email are synchronized.
$randomizerSettings['equal.']['fe_users.'] = [
    // The field 'email' gets the value of field 'username'.
    'email' => 'username',
];

$GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['randomizer'] = $randomizerSettings;
```

## Usage

### Randomize in Database

ATTENTION: By default the database changes are not written to the database! If you really want to randomize the
values in the database, add the `--no-dry-run` option.

Run the cli command:

```shell script
vendor/bin/typo3cms randomizer:randomize
```

Run `vendor/bin/typo3cms randomizer:randomize -h` to see all options. 

### Randomize during export

Run the cli command:

```shell script
vendor/bin/typo3cms randomizer:export mydatabase.sql
```

Run `vendor/bin/typo3cms randomizer:export -h` to see all options. 

## Custom faker providers

Register your provider in your ext_localconf.php:

```php
$GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['randomizer']['faker.']['providers.']['myProvider'] = \Vendor\Namespace\To\MyProvider::class;
```

Write your provider based on `\Faker\Provider\Base`.
There is an example in `\Mhuber84\Randomizer\FakerProviders\ExampleProvider` and `\Mhuber84\Randomizer\FakerProviders\ArrayProvider`.
More informations on custom providers at https://github.com/fzaninotto/Faker#faker-internals-understanding-providers.

## Development

Development is done with [ddev](https://www.ddev.com/).

### Functional tests

There are two functionial tests for the two commands. You can run them with 
```
ddev functional-tests "^11" "7.4"
ddev functional-tests "^11" "8.1"
ddev functional-tests "^12" "8.1"
ddev functional-tests "^11" "8.2"
ddev functional-tests "^12" "8.2"
```

### Code quality

PHP CS Fixer: `ddev exec .Build/bin/php-cs-fixer fix`

## Credits

Many thanks go out to François Zaninotto and his package [fzaninotto/faker](https://github.com/fzaninotto/Faker)!

And also thank you https://github.com/orgs/FakerPHP/people for forking the original package and maintainig it after
the original package was [archived](https://marmelab.com/blog/2020/10/21/sunsetting-faker.html).

Many thanks for the inspiration to this package go out to Georg Ringer and his package [georgringer/gdpr](https://github.com/georgringer/gdpr)!

## Author

<a href="https://marco-huber.de">Marco Huber</a>, <a href="https://twitter.com/mhuber84">@mhuber84</a>, mail@marco-huber.de

Feel free to contribute and send in pull requests or create an issue.
