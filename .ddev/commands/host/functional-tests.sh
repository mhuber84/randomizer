#!/bin/bash

## Description: Run functional tests inside the web container
## Usage: functional-tests "^11" "7.4"
## Example: `ddev functional-tests "^11" "7.4"´ or ´ddev functional-tests "^11" "8.1"` or ´ddev functional-tests "^11" "8.2"`

TEST_TYPO3_VERSION=$1

TEST_PHP_VERSION=$2
OLD_PHP_MAJOR_VERSION=$(ddev exec "php -r 'echo PHP_MAJOR_VERSION;'")
OLD_PHP_MINOR_VERSION=$(ddev exec "php -r 'echo PHP_MINOR_VERSION;'")
OLD_PHP_VERSION="$OLD_PHP_MAJOR_VERSION.$OLD_PHP_MINOR_VERSION"

TESTSCRIPT='typo3DatabaseName="testdb" typo3DatabaseUsername="root" typo3DatabasePassword="root" typo3DatabaseHost="db" typo3DatabasePort="3306" composer'
TESTSCRIPT_PHP="ci:tests:functional-php8"
if [[ "$TEST_PHP_VERSION" == "7.4" ]]; then
  TESTSCRIPT_PHP="ci:tests:functional-php7"
fi

if [[ "$TEST_PHP_VERSION" != "$OLD_PHP_VERSION" ]]; then
  ddev config --php-version "$TEST_PHP_VERSION"
  ddev restart
fi

rm -f composer.lock
ddev composer require typo3/minimal="$TEST_TYPO3_VERSION"
ddev composer install

ddev exec $TESTSCRIPT $TESTSCRIPT_PHP

ddev composer remove typo3/minimal
rm -f composer.lock

if [[ "$TEST_PHP_VERSION" != "$OLD_PHP_VERSION" ]]; then
  ddev config --php-version "$OLD_PHP_VERSION"
  ddev restart
fi

exit
