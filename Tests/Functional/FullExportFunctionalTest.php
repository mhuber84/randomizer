<?php

namespace Mhuber84\Randomizer\Tests\Functional;

use Doctrine\DBAL\Query\QueryBuilder;
use TYPO3\TestingFramework\Core\Functional\FunctionalTestCase;

/**
 * Class ExportFunctionalTest
 * https://de.slideshare.net/cpsitgmbh/functional-tests-with-typo3
 *
 * rm -r composer.lock .Build
 * ddev composer require typo3/minimal:9.5
 * ddev exec 'typo3DatabaseName="randomizer" typo3DatabaseUsername="root" typo3DatabasePassword="root" typo3DatabaseHost="db" typo3DatabasePort="3306" TYPO3_PATH_WEB="$PWD/.Build/Web" $PWD/.Build/bin/phpunit -c $PWD/.Build/vendor/nimut/testing-framework/res/Configuration/FunctionalTests.xml $PWD/.Build/Web/typo3conf/ext/randomizer/Tests/Functional/FullExportFunctionalTest.php'
 */
class FullExportFunctionalTest extends FunctionalTestCase
{

    /**
     * Ensure your extension is loaded
     *
     * @var array
     */
    protected array $testExtensionsToLoad = [
        'typo3conf/ext/randomizer',
        ];

    protected array $configurationToUseInTestInstance = [
        'EXT' => [
            'extConf' => [
                'randomizer' => 'a:2:{s:6:"faker.";a:3:{s:8:"mapping.";a:1:{s:9:"fe_users.";a:1:{s:8:"username";s:8:"userName";}}s:7:"unique.";a:1:{s:9:"fe_users.";a:1:{s:8:"username";b:1;}}s:6:"locale";s:5:"de_DE";}s:6:"equal.";a:1:{s:9:"fe_users.";a:1:{s:5:"email";s:8:"username";}}}',
            ],
        ],
        'EXTENSIONS' => [
            'randomizer' => [
                'faker.' => [
                    'mapping.' => [
                        'fe_users.' => [
                            'username' => 'userName',
                        ],
                    ],
                    'unique.' => [
                        'fe_users.' => [
                            'username' => true,
                        ],
                    ],
                    'locale' => 'de_DE',
                ],
                'equal.' => [
                    'fe_users.' => [
                        'email' => 'username',
                    ],
                ],
            ],
        ],
        'SYS' => [
            'trustedHostsPattern' => '.*.*',
            'devIPmask' => '*',
            'displayErrors' => 1,
        ],
    ];

    protected $fixturePath;

    protected $originalData;
    protected $randomizedData;

    protected function setUp(): void
    {
        parent::setUp();

        $this->fixturePath = __DIR__ . '/Fixtures/';

        // Import own fixtures
        $this->importCSVDataSet($this->fixturePath . 'Database.csv');

        // Set up the frontend!
        $this->setUpFrontendRootPage(1);

        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $this->getConnectionPool()->getQueryBuilderForTable('fe_users');
        $this->originalData = $queryBuilder->select('*')->from('fe_users')->executeQuery()->fetchAssociative();

        $databaseParams = $queryBuilder->getConnection()->getParams();
        $typo3Environment = 'typo3DatabaseName="' . $queryBuilder->getConnection()->getDatabase() . '" typo3DatabaseUsername="' . $databaseParams['user'] . '" typo3DatabasePassword="' . $databaseParams['password'] . '" typo3DatabaseHost="' . $databaseParams['host'] . '" typo3DatabasePort="' . $databaseParams['host'] . '" TYPO3_PATH_WEB="' . $this->getInstancePath() . '" ';
        shell_exec($typo3Environment . $this->getInstancePath() . '/../../../../../bin/typo3 randomizer:export -q --full-export ' . $this->getInstancePath() . '/testdump.sql');
        shell_exec('mysql -u"' . $databaseParams['user'] . '" -p"' . $databaseParams['password'] . '" -h"' . $databaseParams['host'] . '" -P3306 "' . $queryBuilder->getConnection()->getDatabase() . '" < ' . $this->getInstancePath() . '/testdump.sql');
        unlink($this->getInstancePath() . '/testdump.sql');

        /** @var QueryBuilder $queryBuilder2 */
        $queryBuilder2 = $this->getConnectionPool()->getQueryBuilderForTable('fe_users');
        $this->randomizedData = $queryBuilder2->select('*')->from('fe_users')->executeQuery()->fetchAssociative();
    }

    /**
     * username is changed after command is executed
     *
     * @test
     */
    public function usernameIsChanged()
    {
        self::assertNotEquals(
            $this->originalData['username'],
            $this->randomizedData['username']
        );
    }

    /**
     * email equals username after command is executed
     *
     * @test
     */
    public function emailEqualsUsername()
    {
        self::assertEquals(
            $this->randomizedData['username'],
            $this->randomizedData['email']
        );
    }
}
